const router = require('express').Router();
const { formParseMiddleware } = require('../common/middlewares');
const dlController = require('./subRoutes/dl');

router.post('/readDL', formParseMiddleware, dlController.readDL);

module.exports = router;
