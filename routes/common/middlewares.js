const formidable = require('formidable');
const fs = require('fs');
const logger = require('kubera-logger').appLogger();
const { v4: uuidv4 } = require('uuid');
const out = require('@lib/apiout');

const formParseMiddleware = (req, res, next) => {
  let imagePath = '';
  let form;
  logger.log(logger.LOG_LEVELS.INFO, 'form parse middleware');
  let invalid = false;
  if (!req.headers['content-type'] || req.headers['content-type'].match('json')) {
    return next();
  }
  const method = req.method.toLowerCase();
  if (method === 'post' || method === 'put'
    || method === 'patch' || method === 'delete') {
    form = new formidable.IncomingForm();
    form.hash = 'md5';
    form.multiples = true;
    form.on('fileBegin', (name, file) => {
      logger.log(
        logger.LOG_LEVELS.INFO,
        `fileBegin : ${JSON.stringify(file)}`,
      );
      imagePath = file.path = `/tmp/${Math.floor(new Date())}${uuidv4()}.jpg`;
      req.imagePath = imagePath;
    });

    form.on('file', name => logger.log(logger.LOG_LEVELS.INFO, `file uploaded ${name}`));
    form.on('error', err => logger.log(logger.LOG_LEVELS.ERROR, err));

    logger.log(logger.LOG_LEVELS.INFO, 'Parsing form');
    form.parse(req, (err, fields, files) => {
      if (err != null) {
        logger.log(logger.LOG_LEVELS.INFO, `formidable:parse ${err}`);
        return out.error(res, 500, 'upload error');
      }
      req.body = fields;
      req.files = files;
      Object.keys(files).forEach((file) => {
        req.body[file] = '--masked--';
        if (files[file].size < 12 * 1000 * 1000) return;

        invalid = true;
        out.error(res, out.codes.INVALIDREQ, 'image size cannot be greater than 12MB');
        if (files[file].path) {
          fs.unlink(files[file].path, (errUnlink) => {
            logger.log(
              logger.LOG_LEVELS.INFO,
              `deleting larger image, err:${JSON.stringify(errUnlink)}`,
            );
          });
        }
      });
      logger.log(logger.LOG_LEVELS.INFO, JSON.stringify(req.files));
      return logger.log(logger.LOG_LEVELS.INFO, 'parsed');
    });

    form.on('end', () => {
      if (invalid) return;
      next();
    });
    return null;
  }
  return next();
};

module.exports = { 
  formParseMiddleware,
}
