const config = require('nconf').get();

module.exports = {
  blacklist: {
    request: {
      headers: [
        '$.appkeydigest',
        '$.appkey',
        '$.appKey',
      ],
      body: [
        '$.appkeydigest',
        '$.appkey',
        '$.appKey',
      ],
    },
    response: {
      body: [
      ],
    },
  },
  filename: config.REQ_LOG,
  extraProperties: {
    request: ['process'],
    response: ['process'],
  },
};
