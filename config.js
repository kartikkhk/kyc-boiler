module.exports = {
  default: {
    REQ_LOG: '/var/log/HV/requests/usa-ocr.log',
    NODE_LOG: '/var/log/HV/node/node.log',
    S3_BUCKET: 'kyc-hyperverge-co',
    DOCUMENT_PATH: '/tmp/',
  },
  development: {
    PORT: 3000,
    REQ_LOG: './logs/req.log',
    NODE_LOG: './logs/node.log',
  },
};
