const express = require('express');
const os = require('os');
const helmet = require('helmet');
const { requestLogger, appLogger: AppLogger } = require('kubera-logger');
const cookieParser = require('cookie-parser');
const reqLogOpts = require('./common/reqLogConfig');
const { NODE_LOG } = require('nconf').get();
const v1Routes = require('./routes/index');

const app = express();

app.use(helmet.hidePoweredBy());
app.use(helmet.ieNoOpen());
app.use(helmet.dnsPrefetchControl());

app.use(cookieParser());

requestLogger(app, 'usa-ocr', reqLogOpts);
const logger = new AppLogger(os.hostname(), 'usa-ocr', 'node', app, true, NODE_LOG);

app.get('/', (req, res) => res.json({ msg: 'AoK' }));
app.use('/', v1Routes);


logger.log(logger.LOG_LEVELS.INFO, 'Spawning usa-ocr server');
module.exports = app;
